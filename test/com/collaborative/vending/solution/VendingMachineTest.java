package com.collaborative.vending.solution;

import static org.junit.Assert.fail;

import java.util.EmptyStackException;

import org.junit.Test;

import com.collaborative.vending.solution.stacks2D.Item;
import com.collaborative.vending.solution.stacks2D.VendingMachine;

public class VendingMachineTest {

	public static final int DEFAULT_X_SIZE = 3;
	public static final int DEFAULT_Y_SIZE = 3;
	
	@Test
	public void emptyMachineThrowsCorrectExceptions() {
		VendingMachine emptyMachine = new VendingMachine(DEFAULT_X_SIZE,DEFAULT_Y_SIZE);
		
		try{
			emptyMachine.purchaseItem(3, 0, 1.00);
			fail("An exception should have been thrown for selecting an item out of bounds");
		} catch( ArrayIndexOutOfBoundsException e ){
			//good - continue
		} catch( Exception e ){
			fail("Did not catch the correct exception for an empty stack!");
		}
		
		try{
			emptyMachine.purchaseItem(0, 0, 1.00);
			fail("An exception should have been thrown for an empty stack!");
		} catch ( EmptyStackException e ){
			assert(true);
		} catch ( Exception e ){
			fail("Did not catch the correct exception for an empty stack");
		}
	}
	
	@Test
	public void loadedMachineThrowsCorrectExceptions(){
		VendingMachine loadedMachine = new VendingMachine(DEFAULT_X_SIZE,DEFAULT_Y_SIZE);
		Item soda = new Item(1.25,"Pepsi");
		
		loadedMachine.addItem(0, 0, soda);
		
		try{
			loadedMachine.purchaseItem(0,0,1.00);
			fail("An exception should have been thrown for not putting in enough money!");
 		} catch( IllegalArgumentException e ){
 			//good - continue
 		} catch( Exception e ){
 			fail("The wrong exception was thrown!");
 		}
		
		loadedMachine.purchaseItem(0, 0, 1.25);
		
		try{
			loadedMachine.purchaseItem(0, 0, 1.25);
			fail();
		} catch( EmptyStackException e ){
			assert(true);
		} catch( Exception e ){
			fail("The wrong exception was thrown!");
		}
		
	}

}
