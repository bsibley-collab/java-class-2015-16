package com.collaborative.change;
import java.util.Scanner;


public class Application {

	public static double selectItem(Scanner sc){
		while(true){
			System.out.println("Enter a (#) for an item you want:");
			System.out.println("1) Candy      : $1.25");
			System.out.println("2) Soda       : $1.50");
			System.out.println("3) Mixed Nuts : $2.13");
			int number = sc.nextInt();
			switch (number){
			case 1:
				System.out.println("You chose Candy.");
				return 1.25;
			case 2:
				System.out.println("You chose Soda.");
				return 1.50;
			case 3:
				System.out.println("You chose Mixed Nuts.");
				return 2.13;
			default:
				System.out.println("You did not enter a valid number!");
			}
		}
	}

	public static double inputMoney(Scanner sc){
		System.out.println();
		System.out.print("Please enter a value to pay for this item: ");

		return sc.nextDouble();
	}

	public static void calculateChange(double cost, double money){
		if ( money >= cost ){
			double remainder = money - cost;
			int quarters = 0;
			int dimes = 0;
			int nickels = 0;
			int pennies = 0;
			while( remainder >= 0.25 ){
				quarters++;
				remainder -= .25;
			}
			while( remainder >= 0.10){
				dimes++;
				remainder -= .10;
			}
			while( remainder >= 0.05){
				nickels++;
				remainder -= .05;
			}
			while( remainder >= 0.01){
				pennies++;
				remainder -= .01;
			}
			System.out.println("Your change is "+quarters+" quarters, "+dimes+" dimes, " + nickels +" nickels, " + pennies +" pennies.");
		}
		else{
			System.out.println("You did not put enough money into the machine!");
		}
	}

	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);

		double cost = selectItem(sc);
		double money = inputMoney(sc);
		calculateChange(cost,money);
		
	}
}
