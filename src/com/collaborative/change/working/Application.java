package com.collaborative.change.working;

import java.util.Scanner;

public class Application {
	//while loops, switch-case, arithmetic, methods
	public static void main(String[] args){
		//A class provided by Java that we use to grab input from the console
		//System.in is the default input stream provided by the console
		Scanner sc = new Scanner(System.in);
		//Choose an item
		System.out.println("Enter a (#) for an item you want:");
		System.out.println("1) Candy      : $1.25");
		System.out.println("2) Soda       : $1.50");
		System.out.println("3) Mixed Nuts : $2.13");
		//Read item choice
		int choice = sc.nextInt(); //We read the next int value that the user enters into the console
		double cost = calculateCost(choice); //Noice that calculateCost is italiziced - that is because it is a static method
		
		while(true){ //a while loop only exits when the condition is false, putting in 'true' as its condition turns it into an infinite loop!
			//Read money
			System.out.println("Please input money...");
			
			double money = sc.nextDouble();
			
			boolean goodToGo = calculateMoney(money,cost);
			
			if ( goodToGo ) break; //To break from our infinite loop (or any loop), we need to call the break keyword
		}
	}
	
	/*
	 * Breaking down this method signature:
	 * public - the scope
	 * static - this method is shared between all instances of this class - you can access this method without instantiating the "Application" class!
	 *        * We need to use static in THIS case, because we're accessing it from another static method (main)
	 * double - the return type of this method
	 * calculateCost - the name of our method
	 * (int choice) - our parameter of type int
	 */
	
	public static double calculateCost(int choice){
		double cost = 0.0;
		/*
		 * Syntax of a switch-case structure:
		 * switch (parameter){
		 * case (value):
		 *    ...
		 *    break;
		 * case (value):
		 *    ...
		 *    break;
		 * ....
		 * default: //this is optional
		 *    ...
		 *    break;
		 * }
		 */
		switch (choice){
		case 1:
			System.out.println("You chose Candy!");
			cost = 1.25;
			break; //If this is not placed here, it continues to run through the rest of the cases - including default!
		case 2:
			System.out.println("You chose soda!");
			cost = 1.50;
			break;
		case 3:
			System.out.println("You chose nuts!");
			cost = 2.13;
			break;
		default:
			System.out.println("You chose something wrong - so we chose candy");
			cost = 1.25;
			break;
		}
		return cost;
	}
	
	/**
	 * Our method to calculate our change and output it to the console
	 * @param money - value that the user gives us
	 * @param cost - value of the item's cost that the user chose
	 * @return a boolean value: false if the user did not give us enough money, true if they did!
	 */
	public static boolean calculateMoney(double money, double cost){
		if (money < cost) { //If the user did not put enough money in...
			System.out.println("You didn't enter enough money!");
			return false;
		}
		else{ //User gave us enough money, so let's count the change
			double difference = money - cost;
			int quarters = 0;
			int dimes = 0;
			int nickels = 0;
			int pennies = 0;
			while ( difference >= 0.25 ){ //while we can still count another quarter out of the change
				quarters++; //the ++ operator is the equivalent of quarters = quarters + 1;
				difference -= 0.25; // the -= operator is the equivalent of difference = difference - 0.25;
			}
			while ( difference >= 0.10 ){
				dimes++;
				difference -= 0.10;
			}
			while ( difference >= 0.05 ){
				nickels++;
				difference -= 0.05;
			}
			while ( difference >= 0.01 ){
				pennies++;
				difference = 0.01;
			}
			
			System.out.println("You recieved back " + quarters + " quarters, " 
			                                        + dimes + " dimes, " 
					                                + nickels + " nickels, " 
			                                        + pennies + " pennies.");
			return true;
		}
	}
}
