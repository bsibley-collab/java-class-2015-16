package com.collaborative.vending.week4;

import java.util.ArrayList;
import java.util.List;

public class VendingMachine {

	public static final int DEFAULT_ROW_SIZE = 4;
	
	List<ItemStack> stacks;
	int rowSize;
	
	public VendingMachine(){
		stacks = new ArrayList<ItemStack>();
		rowSize = DEFAULT_ROW_SIZE;
	}
	
	public VendingMachine(List<ItemStack> stacks, int rowSize){
		this.stacks = stacks;
		this.rowSize = rowSize;
	}
}
