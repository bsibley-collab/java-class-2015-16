package com.collaborative.vending.week4;

import java.util.Stack;

public class ItemStack {
	Stack<Item> items;
	
	public ItemStack(){
		items = new Stack<Item>();
	}
	
	public ItemStack(Stack<Item> items){
		this.items = items;
	}
}
