package com.collaborative.vending.week7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Application {

	public static void main(String[] args) {
		// Create VendingMachine and put in its items
		VendingMachine machine;
		try {
			machine = new VendingMachine(readMap());
			
			// Begin console menu loop		
			while(true){
				//// TODO: Print Menu
				System.out.println(machine.toString());
				
				//// TODO: Read user money
				//// Read User menu selection
				//// TODO: Validate both values
				//// TODO: Get & Display item from VendingMachine if valid
				//// TODO: Update Qty of Item if valid
				//// TODO: Finally, print change returned
				break;
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

	private static Map<Integer,VendingMachineItem> generateMap(){
		Map<Integer,VendingMachineItem> myMap = new HashMap<>();
		
		myMap.put(new Integer(11), new VendingMachineItem("Pepsi",1.25,5));
		return myMap;
	}
	
	private static Map<Integer,VendingMachineItem> readMap() throws NumberFormatException, IOException{
		Map<Integer,VendingMachineItem> items = new TreeMap<Integer,VendingMachineItem>();
		
		//Grab existing file
		File file = new File("src/com/collaborative/vending/week6/items.txt");
		
		//Instantiate reader and buffered reader
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		
		//While there's more to read from the file
		while( br.ready() ){
			line = br.readLine(); //Read one line in
			String[] attributes = line.split(","); //We parse on commas in this example
			VendingMachineItem item = new VendingMachineItem(attributes[1].trim(), Double.parseDouble(attributes[2].trim()), Integer.parseInt(attributes[3].trim())); //Add values to our custom object
			items.put(new Integer(attributes[0].trim()), item);
		}
		fr.close(); //Always close your streams
		br.close();
		
		return items;
	}
	
	//TODO: Implement the writing functionality for our vending machine map
	private static void writeMap(){
		
	}
}
