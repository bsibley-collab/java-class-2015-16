package com.collaborative.vending.solution.stacks2D;

import java.util.EmptyStackException;
import java.util.Stack;

public class VendingMachine {

	private Stack<Item>[][] stacks;
	
	/**
	 * Instantiate the object and make sure each position
	 * has been instantiated too
	 * @param _x
	 * @param _y
	 */
	@SuppressWarnings("unchecked")
	public VendingMachine(int _x, int _y){
		stacks = new Stack[_x][_y];
		for(int x = 0; x < _x; x++){
			for(int y = 0; y < _y; y++){
				stacks[x][y] = new Stack<Item>();
			}
		}
	}
	
	/**
	 * Instantiate the object with a preset
	 * @param loadedStacks
	 */
	public VendingMachine(Stack<Item>[][] loadedStacks){
		stacks = loadedStacks;
	}
	
	/*** Accessor Methods ***/
	
	public int getStackWidth(){
		return stacks.length;
	}
	
	public int getStackHeight(){
		return stacks[0].length;
	}
	
	public Item getItem(int x, int y){
		return stacks[x][y].peek();
	}
	
	/*** Mutator Methods ***/
	
	public void addItem(int x, int y, Item item){
		stacks[x][y].push(item);
	}
	
	public Item purchaseItem(int x, int y, double userAmount){
		//Not informative enough on why it could fail
		/*if (  validSelection(x,y) 
		   && !stacks[x][y].isEmpty() 
		   && userAmount >= stacks[x][y].peek().getPrice() ){
			return stacks[x][y].pop();
		  else return null;
		}*/
		//Throwing exceptions gives us more information than just returning null
		if ( !validSelection(x,y) ) 
			throw new ArrayIndexOutOfBoundsException("Did not select a valid stack!");
		else if ( stacks[x][y].isEmpty() ) 
			throw new EmptyStackException();
		else if ( userAmount < stacks[x][y].peek().getPrice() ) 
			throw new IllegalArgumentException("User did not enter in enough money!");
		else 
			return stacks[x][y].pop();
	}
	
	/*** Instance Methods ***/
	
	@Override
	public String toString(){
		StringBuilder str = new StringBuilder();
		for( int i = 0; i < this.getStackWidth(); i++ ){
			for ( int j = 0; j < this.getStackHeight(); j++ ){
				str.append("[");
				str.append(stacks[i][j].peek().toString());
				str.append("->count:");
				str.append(stacks[i][j].size());
				str.append("]");
			}
			str.append("\n");
		}
		return str.toString();
	}
	
	/*** Helper Methods ***/
	
	private boolean validSelection(int x, int y){
		if ( x >= 0 && x < stacks.length && y >= 0 && y < stacks[0].length ){
			return true;
		} else return false;
	}
}
