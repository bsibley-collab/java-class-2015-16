package com.collaborative.vending.solution.stacks2D;

public class Item {
	private final double price;
	private final String name;
	
	public Item(double price, String name){
		this.price = price;
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString(){
		return name + ":" + price;
	}
}
