package com.collaborative.vending.solution.listsThenStacks;

import java.util.ArrayList;
import java.util.List;

public class VendingMachine {

	private List<ItemStack> stacks;
	private int rowSize;
	
	/**
	 * Instantiate the object and make sure each position
	 * has been instantiated too
	 * @param _x
	 * @param _y
	 */
	public VendingMachine(int rowSize, int numRows){
		this.rowSize = rowSize;
		stacks = new ArrayList<ItemStack>();
		int actualSize = rowSize*numRows;
		for(int i = 0; i < actualSize; i++){
			stacks.add(new ItemStack());
		}
	}
	
	/**
	 * Instantiate the object with a preset
	 * @param loadedStacks
	 */
	public VendingMachine(List<ItemStack> stacks, int rowSize){
		this.rowSize = rowSize;
		this.stacks = stacks;
	}
	
	/*** Accessor Methods ***/
	
	public int getRowSize(){
		return rowSize;
	}
	
	public String getItemDescription(int row, int colm){
		int index = getIndex(row,colm);
		return stacks.get(index).getItemDescription();
	}
	
	/*** Mutator Methods ***/
	
	public void addItem(int row, int colm, Item item){
		int index = getIndex(row,colm);
		stacks.get(index).addItem(item);
	}
	
	public Item purchaseItem(int x, int y, double userAmount){
		int index = getIndex(x,y);
		if ( userAmount >= stacks.get(index).getItemPrice() ){
			return stacks.get(index).removeItem();
		} else return null;
	}
	
	private int getIndex(int row,int colm){
		return (rowSize*(row)+colm);
	}
	/*** Instance Methods ***/
	
	@Override
	public String toString(){
		StringBuffer str = new StringBuffer();
		int colm = 0;
		for( ItemStack stack : stacks ){
			str.append("["+stack.toString()+"]");
			colm++;
			if ( colm == rowSize ){
				str.append("\n");
				colm = 0;
			}
		}
		return str.toString();
	}
}
