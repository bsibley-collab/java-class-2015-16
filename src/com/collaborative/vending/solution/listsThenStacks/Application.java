package com.collaborative.vending.solution.listsThenStacks;

public class Application {

	public static void main(String[] args){
		
		VendingMachine machine = loadVendingMachine();
		System.out.println(machine.toString());
		
		machine.getItemDescription(0, 0);
	}
	
	private static VendingMachine loadVendingMachine(){
		VendingMachine machine = new VendingMachine(3,3);
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				switch(i){
				case 0:
					if(j==0)for(int times=0;times<5;times++)machine.addItem(i, j, getSoda("Pepsi"));
					if(j==1)for(int times=0;times<5;times++)machine.addItem(i, j, getSoda("Diet Pepsi"));
					if(j==2)for(int times=0;times<5;times++)machine.addItem(i, j, getSoda("Mountain Dew"));
					break;
				case 1:
					if(j==0)for(int times=0;times<5;times++)machine.addItem(i, j, getChips("Original"));
					if(j==1)for(int times=0;times<5;times++)machine.addItem(i, j, getChips("Sour Cream"));
					if(j==2)for(int times=0;times<5;times++)machine.addItem(i, j, getChips("Nacho Cheese"));
					break;
				case 2:
					if(j==0)for(int times=0;times<5;times++)machine.addItem(i, j, getSnack("Mixed Nuts"));
					if(j==1)for(int times=0;times<5;times++)machine.addItem(i, j, getSnack("Reese's Cups"));
					if(j==2)for(int times=0;times<5;times++)machine.addItem(i, j, getSnack("KitKat"));
					break;
				}
			}
		}
		return machine;
	}
	
	private static Item getSoda(String name){
		return new Item(1.25,name);
	}
	
	private static Item getChips(String name){
		return new Item(.90,name);
	}
	
	private static Item getSnack(String name){
		return new Item(1.00,name);
	}
}
