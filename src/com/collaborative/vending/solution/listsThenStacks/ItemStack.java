package com.collaborative.vending.solution.listsThenStacks;

import java.util.Stack;

public class ItemStack {
	private Stack<Item> items;
	
	public ItemStack(){
		items = new Stack<Item>();
	}
	
	public ItemStack(Stack<Item> items){
		this.items = items;
	}
	
	public int size(){
		return items.size();
	}
	
	public Item removeItem(){
		return items.pop();
	}
	
	public String getItemDescription(){
		return items.peek().toString();
	}
	
	public double getItemPrice(){
		return items.peek().getPrice();
	}
	
	public void addItem(Item item){
		items.push(item);
	}
	
	@Override
	public String toString(){
		return items.peek().toString()+"->count="+this.size();
	}
}
