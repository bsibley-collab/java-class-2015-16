package com.collaborative.vending.week5;

public class VendingMachineItem {
	
	private String name;
	private double price;
	private int qty;

	public VendingMachineItem(String name, double price, int qty) {
		this.name = name;
		this.price = price;
		this.qty = qty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public void takeItem() {
		qty--;
	}

	public int getQty() {
		return qty;
	}
	
	@Override
	public String toString(){
		return "{ " + name + ", $" + price + ", " + qty + " }";
	}
}
