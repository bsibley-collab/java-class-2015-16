package com.collaborative.vending.week5;

import java.util.Map;
import java.util.Map.Entry;

public class VendingMachine {
	private Map<Integer, VendingMachineItem> items;
	
	public VendingMachine(Map<Integer, VendingMachineItem> items){
		this.items=items;
	}
	
	public VendingMachineItem getItem(int key){
		if ( items.containsKey(key) ){
			return items.get(key);
		} throw new IllegalArgumentException("No matching key");
	}
	
	public boolean updateItem(int key){
		try{
			VendingMachineItem item = this.getItem(key);
			if ( item.getQty() > 0 ){
				item.takeItem();
				return true;
			}
		} catch ( IllegalArgumentException e){
			//false
		}
		return false;
	}
	
	public String calculateChange(int key, double money){
		try{
			VendingMachineItem item = this.getItem(key);
			double cost = item.getPrice();
			if ( money >= cost ){
				double remainder = money - cost;
				int quarters = 0;
				int dimes = 0;
				int nickels = 0;
				int pennies = 0;
				while( remainder >= 0.25 ){
					quarters++;
					remainder -= .25;
				}
				while( remainder >= 0.10){
					dimes++;
					remainder -= .10;
				}
				while( remainder >= 0.05){
					nickels++;
					remainder -= .05;
				}
				while( remainder >= 0.01){
					pennies++;
					remainder -= .01;
				}
				return "Your change is "+quarters+" quarters, "+dimes+" dimes, " + nickels +" nickels, " + pennies +" pennies.";
			}
			else{
				return "You did not put enough money into the machine!";
			}
		} catch( IllegalArgumentException e ){
			return "This machine is out of that item!";
		}
	}
	
	@Override
	public String toString(){
		String str = "";
		
		for( Entry<Integer,VendingMachineItem> entry : items.entrySet() ){
			str += entry.getKey() + " : " + entry.getValue().toString() + "\n";
		}
		
		return str;
	}
}
