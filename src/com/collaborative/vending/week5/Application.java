package com.collaborative.vending.week5;

import java.util.HashMap;
import java.util.Map;

public class Application {

	public static void main(String[] args) {
		// Create VendingMachine and put in its items
		VendingMachine machine = new VendingMachine(generateMap());
		
		System.out.println(machine.toString());
		// Begin console menu loop
		//// Print Menu
		//// Read user money
		//// Read User menu selection
		//// Validate both values
		//// Get & Display item from VendingMachine if valid
		//// Update Qty of Item if valid
		//// Finally, print change returned

	}

	private static Map<Integer,VendingMachineItem> generateMap(){
		Map<Integer,VendingMachineItem> myMap = new HashMap<>();
		
		myMap.put(new Integer(11), new VendingMachineItem("Pepsi",1.25,5));
		
		return myMap;
	}
}
