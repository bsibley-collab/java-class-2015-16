package com.collaborative.examples;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class IteratorApplication {

	public static void main(String[] args){
		setIterator();
		mapIterator();
	}

	private static void setIterator(){
		Set<Integer> setOfIntegers = new HashSet<Integer>();
		setOfIntegers.add(new Integer(1));
		setOfIntegers.add(new Integer(2));
		setOfIntegers.add(new Integer(3));

		Iterator<Integer> it = setOfIntegers.iterator();

		while(it.hasNext()){
			Integer item = it.next();
			System.out.println(item.toString());
		}

		for( Integer item : setOfIntegers ){
			System.out.println(item.toString());
		}
	}

	private static void mapIterator(){
		Map<Integer,Object> myMap = new HashMap<Integer,Object>();

		myMap.put(new Integer(1), "Message 1!");
		myMap.put(new Integer(2), "Message 2!");
		myMap.put(new Integer(3), "Message 3!");
		
		Set<Entry<Integer,Object>> entries = myMap.entrySet();

		Iterator<Entry<Integer,Object>> it2 = entries.iterator();

		while( it2.hasNext() ){
			Entry<Integer,Object> item = it2.next();
			System.out.println(item.getValue().toString());
		}

		for( Entry<Integer,Object> entry : entries ){
			System.out.println(entry.getValue().toString());
		}
	}
}
