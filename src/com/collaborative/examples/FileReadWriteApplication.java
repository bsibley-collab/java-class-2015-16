package com.collaborative.examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This application closely follows the guide here: http://www.tutorialspoint.com/java/java_files_io.htm
 * @author bsibley
 *
 */
public class FileReadWriteApplication {
	
	static class Item{ //Our custom class
		final String name;
		final double price;
		private int qty;
		public Item(String name, double price, int qty){
			this.name = name;
			this.price = price;
			this.qty = qty;
		}
		@Override
		public String toString(){
			return name + ", " + price + ", " + qty;
		}
	}
	
	public static void main(String[] args) throws IOException{
		List<Item> items = new ArrayList<Item>();
		
		//Grab existing file
		File file = new File("src/com/collaborative/examples/items.txt");
		
		//Instantiate reader and buffered reader
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		
		//While there's more to read from the file
		while( br.ready() ){
			line = br.readLine(); //Read one line in
			String[] attributes = line.split(","); //We parse on commas in this example
			Item item = new Item(attributes[0].trim(), Double.parseDouble(attributes[1].trim()), Integer.parseInt(attributes[2].trim())); //Add values to our custom object
			items.add(item); //Add each item to a list
		}
		
		//Prepare to Write out
		StringBuffer fileOutText = new StringBuffer();
		for( Item i : items ){
			System.out.println(i.toString()); //Print to console so that user can see what we read (and what we're about to write)
			fileOutText.append(i.toString()); //Add .toString() value of each item to our string buffer
			fileOutText.append("\n"); //Each item is separated by the new line character
		}
		fr.close(); //Always close your streams
		br.close();
		
		File newFile = new File("src/com/collaborative/examples/items-out.txt"); //We'll call our new file items-out.txt
		newFile.createNewFile(); //Creates it
		FileWriter fw = new FileWriter(newFile);
		fw.write(fileOutText.toString()); //Writes to it
		fw.close(); //Close stream again
	}

}
