package com.collaborative.examples.week9;

import java.util.ArrayList;
import java.util.List;

public class MyBag{
		private int size;
		List<Object> bag;
		public MyBag(){
			bag = new ArrayList<Object>();
			size = 0;
		}
		
		public int size(){
			return size;
		}
		
		public boolean contains(Object obj){
			return bag.contains(obj);
		}
		
		public void add(Object obj){
			if ( !bag.contains(obj) ){
				bag.add(obj);
				size++;
			}
		}
		
		public boolean remove(Object obj){
			if ( bag.contains(obj) ){
				bag.remove(obj);
				size--;
				return true;
			} else return false;
		}
		
		public static String message(){
			return "This is a static message!";
		}
	}