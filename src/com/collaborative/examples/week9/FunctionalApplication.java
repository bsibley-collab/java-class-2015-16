package com.collaborative.examples.week9;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class FunctionalApplication 
{
	public static void main(String args[]){
		MyBag bag = new MyBag();
		Integer commonInteger = new Integer(2);
		Integer uncommonInteger = new Integer(1337);
		bag.add(commonInteger);
		bag.add(new Boolean(false));
		
		Supplier<String> staticMethod = MyBag::message;
		Function<Object,Boolean> containsAction = bag::contains;
		Consumer<Object> addAction = bag::add;
		
		System.out.println("Does my bag have '" + commonInteger + "'? " + containsAction.apply(commonInteger));
		System.out.println("Does my bag have '" + uncommonInteger + "'? " + containsAction.apply(uncommonInteger));
		System.out.println("Adding '" + uncommonInteger + "'..." );
		
		addAction.accept(uncommonInteger);
		
		System.out.println("Does my bag have '" + uncommonInteger + "'? " + containsAction.apply(uncommonInteger));
		
		System.out.println(staticMethod.get());
	}
}


