package com.collaborative.examples;

import java.util.ArrayList;
import java.util.List;

class MyBag{
	private int size;
	private List<Integer> items;
	
	public MyBag(){
		size = 0;
		items = new ArrayList<Integer>();
	}
	
	public int size(){
		return size;
	}
	
	@Override
	public boolean equals(Object obj){
		if (obj instanceof MyBag){
			MyBag that = (MyBag)obj;
			return this.size() == that.size();
		}
		return false;
	}
}

public class EqualsApplication {
	
	public static void main(String[] args){
		int valueOne = 1;
		int valueTwo = 2;
		int valueOneAgain = 1;
		
		String stringOne = "one";
		String stringTwo = "two";
		String stringOneAgain = "one";
		
		MyBag bagOne = new MyBag();
		MyBag bagTwo = new MyBag();
		
		System.out.println("valueOne == valueTwo ? " + (valueOne == valueTwo));
		System.out.println("valueOne == valueOneAgain ? " + (valueOne == valueOneAgain));
		
		System.out.println("stringOne == stringTwo ? " + (stringOne == stringTwo));
		System.out.println("stringOne == stringOneAgain ? " + (stringOne == stringOneAgain));
		
		//This is preferred! No funky behavior
		System.out.println("stringOne.equals(stringOneAgain) ? " + (stringOne.equals(stringOneAgain)));
		
		System.out.println("bagOne == bagTwo ? " + (bagOne == bagTwo));
		System.out.println("bagOne.equals(bagTwo) ? " + (bagOne.equals(bagTwo)));
	}
}
