package com.collaborative.examples.week7;

import java.util.ArrayList;
import java.util.List;

public class MyBag {

	private int size;
	private List<Object> items;
	
	public MyBag(){
		size = 0;
		items = new ArrayList<Object>();
	}
	
	public void add(Object obj){
		if (!contains(obj)){
			items.add(obj);
			size++;
		}
	}
	
	public void remove(Object obj){
		if (contains(obj)){
			items.remove(obj);
			size--;
		}
	}
	
	public boolean contains(Object obj){
		if (items.contains(obj)) return true;
		else return false;
	}
	
	public int size(){
		return size;
	}
	
	@Override
	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append("Size:"+size+" ");
		for( Object item : items ){
			str.append("'");
			str.append(item.toString());
			str.append("' ");
		}
		return str.toString();
	}
}
