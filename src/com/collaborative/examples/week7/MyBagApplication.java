package com.collaborative.examples.week7;

import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MyBagApplication {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		PrintStream output = System.out;
		MyBag bag = new MyBag();
		while(true){
			output.println();
			output.println("Select a menu option:");
			output.println("1) Print MyBag status");
			output.println("2) Add to MyBag");
			output.println("3) Remove from MyBag");
			output.println("4) Exit program.");
			
			try{
				int choice = input.nextInt();
				switch( choice ){
				case 1:
					output.println(bag.toString());
					break;
				case 2:
					addToMyBag(bag,input,output);
					break;
				case 3:
					removeFromMyBag(bag,input,output);
					break;
				case 4:
					System.exit(0);
					break;
				default:
					System.err.println("You did not enter a valid number");
				}
			} catch ( InputMismatchException e){
				System.err.println("You did not enter a valid number");
				input.next();
			}
		}
	}
	
	private static void addToMyBag(MyBag bag, Scanner input, PrintStream output){
		output.println();
		output.println("Select an object to add:");
		output.println("1) Double");
		output.println("2) Integer");
		output.println("3) Boolean");
		output.println("4) String");
		
		try{
			int choice = input.nextInt();
			
			output.println("Enter a value:");
			
			switch( choice ){
			case 1:
				double valueDouble = input.nextDouble();
				bag.add(new Double(valueDouble));
				break;
			case 2:
				int valueInt = input.nextInt();
				bag.add(new Integer(valueInt));
				break;
			case 3:
				boolean valueBoolean = input.nextBoolean();
				bag.add(new Boolean(valueBoolean));
				break;
			case 4:
				String valueString = input.next();
				bag.add(new String(valueString));
				break;
			default:
				System.err.println("You did not enter a valid number");
			}
		} catch ( InputMismatchException e){
			System.err.println("You did not enter valid input");
			input.next();
		}
	}
	
	private static void removeFromMyBag(MyBag bag, Scanner input, PrintStream output){
		output.println();
		output.println("Select an object to remove:");
		output.println("1) Double");
		output.println("2) Integer");
		output.println("3) Boolean");
		output.println("4) String");
		
		try{
			int choice = input.nextInt();
			
			output.println("Enter a value:");
			
			switch( choice ){
			case 1:
				double valueDouble = input.nextDouble();
				bag.remove(new Double(valueDouble));
				break;
			case 2:
				int valueInt = input.nextInt();
				bag.remove(new Integer(valueInt));
				break;
			case 3:
				boolean valueBoolean = input.nextBoolean();
				bag.remove(new Boolean(valueBoolean));
				break;
			case 4:
				String valueString = input.next();
				bag.remove(new String(valueString));
				break;
			default:
				System.err.println("You did not enter a valid number");
			}
		} catch ( InputMismatchException e){
			System.err.println("You did not enter valid input");
			input.next();
		}
	}

}
