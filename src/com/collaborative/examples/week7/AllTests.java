package com.collaborative.examples.week7;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MyBagTest.class, RandomTest.class })
public class AllTests {

}
