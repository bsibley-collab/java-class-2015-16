package com.collaborative.examples.week7;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MyBagTest {

	MyBag emptyBag;
	MyBag fullBag;
	
	Integer intObj = new Integer(1);
	Double doubleObj = new Double(2.0);
	Boolean booleanObj = new Boolean(true);
	Boolean otherBooleanObj = new Boolean(false);
	String stringObj = new String("Hello");
	
	@Before
	public void setUp(){
		emptyBag = new MyBag();
		fullBag = new MyBag();
		fullBag.add(intObj);
		fullBag.add(doubleObj);
		fullBag.add(booleanObj);
		fullBag.add(stringObj);
	}
	
	@Test
	public void size_Empty() {
		assertTrue(emptyBag.size() == 0);
	}
	
	@Test
	public void size_NotEmpty(){
		assertFalse(fullBag.size() == 0);
	}

	@Test
	public void sizeCorrect_Add(){
		MyBag bag = new MyBag();
		bag.add(intObj);
		assertTrue(bag.size() == 1);
	}
	
	@Test
	public void sizeCorrect_Remove(){
		MyBag bag = new MyBag();
		bag.add(intObj);
		bag.add(doubleObj);
		bag.add(booleanObj);
		bag.remove(doubleObj);
		bag.remove(otherBooleanObj);
		assertTrue(bag.size() == 2);
	}
	
	@Test
	public void containsTrue(){
		assertTrue(fullBag.contains(doubleObj));
	}
	
	@Test
	public void containsFalse(){
		assertFalse(fullBag.contains(otherBooleanObj));
	}
}
