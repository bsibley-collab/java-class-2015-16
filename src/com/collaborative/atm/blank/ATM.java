package com.collaborative.atm.blank;

import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.Scanner;

public class ATM {
	
	public final static NumberFormat formatter = NumberFormat.getCurrencyInstance();
	
	public final int SAVINGS_NUM = 1;
	public final int CHECKING_NUM = 2;
	
	private PrintStream output;
	private Scanner input;
	private User user;
	
	public ATM(PrintStream output,Scanner input,User user){
		this.output = output;
		this.input = input;
		this.user = user;
	}
	
	/**
	 * Sets the loggedIn flag on user on true if attempt == pin.
	 * Cannot withdraw or deposit until the user is logged in!
	 * @param user
	 * @param attempt
	 * @return
	 */
	public Message login(User user, int attempt){
		if ( user.login(attempt) ){
			this.user = user;
			return new Message(output,"You successfully logged in.");
		} else{
			return new Message(output,"You did not enter your pin correctly.");
		}
	}
	
	/**
	 * Sets the loggedIn flag on user to false.
	 * @return
	 */
	public Message logout(){
		user.logout();
		return new Message(output,"You successfully logged out.");
	}
	
	/**
	 * Returns a Prompt if user is logged in for withdrawing; otherwise returns null
	 * @param num
	 * @param amount
	 * @return
	 */
	public Prompt<Boolean> withdraw(int num, double amount){
		switch(num){
		case SAVINGS_NUM:
			SavingsAccount savings = user.getSavings();
			if ( savings != null ){
				return new Prompt<Boolean>(s -> savings.withdraw(s), input,
						new Message(output,"Withdrawing '"+formatter.format(amount)+"'..."));
			}
			break;
		case CHECKING_NUM:
			CheckingAccount checking = user.getChecking();
			if ( checking != null ){
				return new Prompt<Boolean>(s -> checking.withdraw(s), input,
						new Message(output,"Withdrawing '"+formatter.format(amount)+"'..."));
			}
			break;
		}
		return null; //not logged in
	}
	
	/**
	 * Returns a Prompt if user is logged in for depositing; otherwise returns null
	 * @param num
	 * @param amount
	 * @return
	 */
	public Prompt<Boolean> deposit(int num, double amount){
		switch(num){
		case SAVINGS_NUM:
			SavingsAccount savings = user.getSavings();
			if ( savings != null ){
				return new Prompt<Boolean>(s -> savings.deposit(s), input,
						new Message(output,"Depositing '"+formatter.format(amount)+"'..."));
			}
		case CHECKING_NUM:
			CheckingAccount checking = user.getChecking();
			if ( checking != null ){
				return new Prompt<Boolean>(s -> checking.deposit(s), input,
						new Message(output,"Depositing '"+formatter.format(amount)+"'..."));
			}
			
		}
		return null; //not logged in
	}
	
	public Message predict(int num, long time){
		int periodsLapsed = (int)(time / Account.PERIOD);
		switch(num){
		case SAVINGS_NUM:
			SavingsAccount savings = user.getSavings();
			if ( savings != null ){
				double change = 0.0;
				for ( int i=0; i < periodsLapsed; i++){
					change += savings.consequence();
				}
				return new Message(output,"Your savings account will accumulate '" + formatter.format(change) + "' in " + periodsLapsed + " periods.");
			}
		case CHECKING_NUM:
			CheckingAccount checking = user.getChecking();
			if ( checking != null ){
				double change = 0.0;
				for ( int i=0; i < periodsLapsed; i++){
					change += checking.consequence();
				}
				return new Message(output,"Your checking account will be charged '"+ formatter.format(change) +"' in " + periodsLapsed +" periods.");
			}
			
		}
		
		return new Message(output,"You need to be logged in before accessing this feature");
	}

	public Message balanceAll() {
		SavingsAccount savings = user.getSavings();
		CheckingAccount checking = user.getChecking();
		if ( savings == null || checking == null ){
			return new Message(output,"You are not logged in!");
		} else return new Message(output, "Savings: $" + formatter.format(savings.balance) + "\nChecking: " + formatter.format(checking.balance));
	}
}
