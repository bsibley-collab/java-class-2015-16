package com.collaborative.atm.blank;

import java.io.PrintStream;

public class Message {
	private PrintStream output;
	private String message;
	
	public Message(PrintStream output, String message){
		this.output = output;
		this.message = message;
	}
	
	public void print(){
		output.println(message);
	}
}
