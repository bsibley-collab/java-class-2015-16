package com.collaborative.atm.blank;

import java.util.Scanner;
import java.util.function.Function;

/**
 * A custom message handler that prints a message and then asks the user for
 * a double value, returning any object R
 * @author bsibley
 *
 * @param <R>
 */
public class Prompt<R> {
	private Function<Double,R> action;
	private Scanner input;
	private Message message;
	
	public Prompt(Function<Double,R> action, Scanner input, Message message){
		this.action = action;
		this.input = input;
		this.message = message;
	}
	
	/**
	 * Print the message and apply the action with user input
	 * @return the return value of action
	 * @precondition: Prompt is instantiated and action accepts a double
	 * @postcondition: Account is mutated if action returns true
	 */
	public R prompt(double amount){
		message.print();
		return action.apply(amount);
	}
}
