package com.collaborative.atm.blank;

public class CheckingAccount extends Account{

	public static final double MIN = -500.00;
	public static final double MAX = 10000.00;
	
	@Override
	public boolean deposit(double amount) {
		if ( balance + amount <= MAX ){
			balance += amount;
			return true;
		} else return false;
	}

	@Override
	public boolean withdraw(double amount) {
		if ( balance - amount >= MIN ){
			balance -= amount;
			return true;
		} else return false;
	}

	@Override
	public double consequence() {
		double fee = -50.00;
		if ( balance < 0.0 && balance + fee >= MIN ){
			return fee;
		} else return 0.0;
	}

}
