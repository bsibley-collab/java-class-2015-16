package com.collaborative.atm.blank;

/**
 * Class to keep track of a User's balance in their account
 * @author bsibley
 */
public abstract class Account {
	
	/**
	 * A period is the amount of time that is required
	 * to elapse for a consequence to be applied to the
	 * account. For this example, it will be one week.
	 */
	public static final long PERIOD = 60 * 60 * 24 * 7;
	
	/**
	 * The current balance of the account
	 */
	protected double balance;
	
	/**
	 * Put a positive amount of money into the account.
	 * @param amount - to deposit
	 * @return false - if exceeds MAX amount allowed;
	 *			true - if balance + amount <= MAX
	 */
	public abstract boolean deposit(double amount);
	
	/**
	 * Take out a positive amount of money from the account
	 * @precondition - amount is positive and Account is instantiated
	 * @postcondition - balance is greater than or equal to zero
	 * @param amount - to withdraw
	 * @return false - if balance - amount < MIN;
	 *          true - if balance - amount >= MIN
	 */
	public abstract boolean withdraw(double amount);
	
	/**
	 * Calculate 
	 * @return
	 */
	public abstract double consequence();
}
