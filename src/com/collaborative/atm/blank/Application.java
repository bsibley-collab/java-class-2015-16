package com.collaborative.atm.blank;

import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {
	static PrintStream output = System.out;
	static Scanner input = new Scanner(System.in);
	
	public static void main(String args[]){
		User user = newUser();
		ATM machine = new ATM(output,input,user);
		
		while(true){
			String menuText = "...\nHello " + user.getName() + ",\n\nPlease select an option:\n1) Login\n2) Deposit\n3)"
					+ "Withdraw\n4) Balance \n5) Predict\n6) Logout\n99) Exit Program"
					+ "\n...";
			int choice = InputHandler.askInt(menuText);
			switch(choice){
			case 1:
				int attempt = InputHandler.askInt("Please enter your pin number:");
				machine.login(user, attempt).print();
				break;
			case 2:
				int typeDeposit = InputHandler.askInt("Which account? (1) Savings, (2) Checking:");
				double amountDeposit = InputHandler.askDouble("How much?");
				Prompt<Boolean> responseDeposit = machine.deposit(typeDeposit,amountDeposit);
				if (responseDeposit != null){
					responseDeposit.prompt(amountDeposit);
					output.println("Transcation was successful.");
				} else {
					output.println("Transaction was not successful. Cannot deposit amount.");
				}
				break;
			case 3:
				int typeWithdraw = InputHandler.askInt("Which account? (1) Savings, (2) Checking:");
				double amountWithdraw = InputHandler.askDouble("How much?");
				Prompt<Boolean> responseWithdraw = machine.withdraw(typeWithdraw,amountWithdraw);
				if (responseWithdraw != null){
					responseWithdraw.prompt(amountWithdraw);
					output.println("Transcation was successful.");
				} else {
					output.println("Transaction was not successful. Cannot withdraw amount.");
				}
				break;
			case 4:
				machine.balanceAll().print();
				break;
			case 5:
				int typePredict = InputHandler.askInt("Which account? (1) Savings, (2) Checking:");
				int periodCount = InputHandler.askInt("How many periods do you want to forecast?");
				machine.predict(typePredict,Account.PERIOD*periodCount).print();
				break;
			case 6:
				machine.logout().print();
				break;
			case 99:
				System.exit(0);
				break;
			default:
				output.println("You did not enter a valid option");
				break;
			}
			
			sleep(1000);
		}
		
	}
	
	private static void sleep(long time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static User newUser(){
		String firstName = InputHandler.askString("Please enter a first name:");
		String lastName = InputHandler.askString("Please enter a last name:");
		int pin = InputHandler.askInt("Please enter a PIN:");
		
		return new User(firstName, lastName, pin);
	}
	
	static class InputHandler{
		String message;
		
		public static String askString(String message){
			while(true){
				output.println(message);
				try{
					String actual = input.next();
					return actual;
				} catch ( InputMismatchException e){
					output.println("Please enter a whole number.");
					input.next();
				}
			}
		}
		
		public static double askDouble(String message) {
			while(true){
				output.println(message);
				try{
					double actual = input.nextDouble();
					return actual;
				} catch ( InputMismatchException e){
					output.println("Please enter a whole number.");
					input.next();
				}
			}
		}

		public static int askInt(String message){
			while(true){
				output.println(message);
				try{
					int actual = input.nextInt();
					return actual;
				} catch ( InputMismatchException e){
					output.println("Please enter a whole number.");
					input.next();
				}
			}
		}
	}
}
