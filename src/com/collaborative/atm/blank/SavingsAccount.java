package com.collaborative.atm.blank;

public class SavingsAccount extends Account {
	
	public static final double MIN = 5.00;
	public static final double MAX = 10000.00;
	
	public SavingsAccount(){
		balance = MIN;
	}
	
	@Override
	public boolean deposit(double amount) {
		if ( balance + amount <= MAX ){
			balance += amount;
			return true;
		} else return false;
	}

	@Override
	public boolean withdraw(double amount) {
		if ( balance - amount >= MIN ){
			balance -= amount;
			return true;
		} else return false;
	}

	@Override
	public double consequence() {
		return balance * 0.005;
	}

}
