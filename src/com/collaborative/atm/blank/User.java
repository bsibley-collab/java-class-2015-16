package com.collaborative.atm.blank;

public class User {
	private String firstName;
	private String lastName;
	private SavingsAccount savings;
	private CheckingAccount checking;
	private int pin;
	private boolean loggedIn;
	
	public User(String firstName, String lastName, int pin){
		this.firstName = firstName;
		this.lastName = lastName;
		savings = new SavingsAccount();
		checking = new CheckingAccount();
		this.pin = pin;
	}
	
	public SavingsAccount getSavings(){
		if ( loggedIn ) return savings;
		else return null;
	}
	
	public CheckingAccount getChecking(){
		if ( loggedIn ) return checking;
		else return null;
	}
	
	public boolean login(int attempt){
		if ( pin == attempt ){
			loggedIn = true;
		} else{
			loggedIn = false;
		}
		return loggedIn;
	}
	
	public void logout(){
		loggedIn = false;
	}

	public String getName() {
		return firstName + " " + lastName;
	}

	public static User blankUser() {
		return new User("Not","Logged in!",-1);
	}
}
