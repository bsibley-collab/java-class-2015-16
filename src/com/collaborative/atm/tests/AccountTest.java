package com.collaborative.atm.tests;

import static org.junit.Assert.*;
import com.collaborative.atm.blank.*;

import org.junit.Before;
import org.junit.Test;

public class AccountTest {

	Account savings1;
	Account checking1;
	
	@Before
	public void setUp() throws Exception {
		savings1 = new SavingsAccount();
		checking1 = new CheckingAccount();
	}

	@Test
	public void withdraw_False() {
		assertFalse(savings1.withdraw(Double.MAX_VALUE));
		assertFalse(checking1.withdraw(Double.MAX_VALUE));
	}

	@Test
	public void withdraw_True(){
		assertTrue(savings1.withdraw(0.0));
		assertTrue(checking1.withdraw(0.0));
	}
	
	@Test
	public void deposit_False(){
		assertFalse(savings1.deposit(Double.MAX_VALUE));
		assertFalse(checking1.deposit(Double.MAX_VALUE));
	}
	
	@Test
	public void deposit_True(){
		assertTrue(savings1.deposit(50.00));
		assertTrue(checking1.deposit(50.00));
	}
}
