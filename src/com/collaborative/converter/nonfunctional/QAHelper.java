package com.collaborative.converter.nonfunctional;

import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

/**
 * A custom object built for our conversion application
 * It can store a question, a list of items, and our desired
 * streams for input and output
 * @author bsibley
 *
 */
public class QAHelper {

	/*** Instance Variables ***/
	
	private Scanner input;
	private PrintStream output;
	private String question;
	private List<UnitItem> items;
	
	/*** Constructor Methods ***/
	
	/**
	 * The full constructor with a list of items to choose from
	 * @param input
	 * @param output
	 * @param question
	 * @param items
	 */
	public QAHelper(Scanner input, PrintStream output, String question, List<UnitItem> items){
		this.input = input;
		this.output = output;
		this.question = question;
		this.items = items;
	}
	
	/**
	 * A constructor that doesn't require the list of items
	 * @param input
	 * @param output
	 * @param question
	 */
	public QAHelper(Scanner input, PrintStream output, String question){
		this.input = input;
		this.output = output;
		this.question = question;
	}
	
	/*** Accessor Methods ***/
	
	public UnitItem get(int index){
		return items.get(index);
	}
	
	/*** Instance Methods ***/
	
	/**
	 * Prints out the question
	 */
	public void ask(){
		output.println(question);
	}
	
	/**
	 * Prints out each item in the items list
	 */
	public void printUnitList(){
		for( int i = 0; i < items.size(); i++ ){
			output.println((i+1) + ": " + items.get(i).toString());
		}
	}
	
	/**
	 * Asks the user to input anything
	 * @return
	 */
	public String answer(){
		return input.next();
	}
	
	/**
	 * Asks the UnitItem at the selected index in our items list 
	 * to apply its function with the double value we pass in
	 * @param index - index in our items list
	 * @param value - double value to convert with
	 * @return converted value
	 */
	public double convertTo(int index, double value){
		return items.get(index).convertTo(value);
	}
	
	public double convertFrom(int index, double value){
		return items.get(index).convertFrom(value);
	}
	
	/**
	 * Forces the user to input an integer value
	 * @return valid integer value
	 */
	public int answerAsInt(){
		while(true){ //Run until a valid answer is returned
			try{
				String consoleInput = input.next();
				int attemptedConverstionInt = Integer.parseInt(consoleInput);
				return attemptedConverstionInt;
			} catch ( NumberFormatException e ){ //The user put a value that was not a whole number
				output.println("Please enter a whole number value (0,1,2,...):");
			}
		}
		
	}
	
	/**
	 * Forces the user to input a double value
	 * @return valid double value
	 */
	public double answerAsDouble(){
		while(true){
			try{
				String consoleInput = input.next();
				double attemptedConverstionInt = Double.parseDouble(consoleInput);
				return attemptedConverstionInt;
			} catch ( NumberFormatException e ){ //The user put a value that was not a whole number
				output.println("Please enter a decimal value (1.23, -0.43,...):");
			}
		}
	}
	
	/*** Static Methods ***/
	
	public static void printAnyList(PrintStream output, Object[] list){
		for( int i = 0; i < list.length; i++ ){
			output.println(list[i]);
		}
	}
}
