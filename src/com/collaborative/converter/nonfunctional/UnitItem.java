package com.collaborative.converter.nonfunctional;

import com.collaborative.converter.nonfunctional.strategy.AbstractUnitStrategy;

public class UnitItem {

	/*** Instance Variables ***/
	
	private String currentType;
	private String convertedToType;
	private String description;
	private AbstractUnitStrategy strategy;
	
	/*** Constructor Methods ***/
	
	public UnitItem(String type, String convertedToType, String description, AbstractUnitStrategy strategy){
		this.currentType = type;
		this.convertedToType = convertedToType;
		this.description = description;
		this.strategy = strategy;
	}
	
	/*** Accessor Methods ***/
	
	public String getType(){
		return currentType;
	}
	
	public String getConvertedToType(){
		return convertedToType;
	}
	
	public String getDescription(){
		return description;
	}
	
	/*** Strategy Methods ***/
	
	public double convertTo(double value){
		return strategy.convertTo(value);
	}
	
	public double convertFrom(double value){
		return strategy.convertFrom(value);
	}
	
	/*** Instance Methods ***/
	
	@Override
	public String toString(){
		return currentType + " : '" + description +"'";
	}
}
