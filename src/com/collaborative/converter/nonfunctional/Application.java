package com.collaborative.converter.nonfunctional;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.collaborative.converter.nonfunctional.QAHelper;
import com.collaborative.converter.nonfunctional.strategy.CelsiusToFahrenheitUnitStrategy;
import com.collaborative.converter.nonfunctional.strategy.EuroToUSDUnitStrategy;
import com.collaborative.converter.nonfunctional.strategy.PintToGallonUnitStrategy;

public class Application {

	public static void main(String[] args){
		//Instantiate our local class variables
		Scanner input = new Scanner(System.in);
		PrintStream output = System.out;
		QAHelper subjectQA = buildSubjectQA(input, output);
		
		//Prompt user to choose a convert subject (Temperature, Money, or Volume)
		subjectQA.printUnitList();
		subjectQA.ask();
		int subjectChoice = subjectQA.answerAsInt();
		
		//Based on the answer, go into the subject and ask them to convert from one unit to another
		UnitItem item;
		switch (subjectChoice){
		case 1:
			item = subjectQA.get(0);
			convertMenu(input, output, item);
			break;
		case 2:
			item = subjectQA.get(1);
			convertMenu(input, output, item);
			break;
		case 3:
			item = subjectQA.get(2);
			convertMenu(input, output, item);
			break;
		}
	}
	
	/**
	 * Method that handles any conversion type based on the QAHelper object
	 * @param input
	 * @param applicationQA
	 */
	private static void convertMenu(Scanner input, PrintStream output, UnitItem item){
		
		QAHelper convertTypeQA = new QAHelper(input, output, "Convert to (1)'"+item.getConvertedToType()+"' or to (2)'"+item.getType()+"'?");
		//Get user selection and show that we got it
		convertTypeQA.ask();
		int selection = convertTypeQA.answerAsInt();
		int adjustedSelection = selection-1;
		System.out.println("You entered '" + selection + "'");

		while(true){
			//Ask user for value to convert
			if ( adjustedSelection == 0 ){
				QAHelper converterQA = new QAHelper(input, output, "Please enter a '"+item.getType()+"' value to convert:");
				converterQA.ask();
				//Get user value as a double
				double originalValue = converterQA.answerAsDouble();
				//Convert user value
				double convertedValue = item.convertTo(originalValue);
				//Print out converted value
				System.out.println("You entered '" + originalValue +"', and after applying the function '" 
						+ item.getDescription() + "', the value returned is '" 
						+ convertedValue +"' in '" + item.getConvertedToType() + "'");
				break;
			} else if ( adjustedSelection == 1 ){ //Inverse
				QAHelper converterQA = new QAHelper(input, output, "Please enter a '"+item.getConvertedToType()+"' value to convert:");
				converterQA.ask();
				//Get user value as a double
				double originalValue = converterQA.answerAsDouble();
				//Convert user value
				double convertedValue = item.convertFrom(originalValue);
				//Print out converted value
				System.out.println("You entered '" + originalValue +"', and after applying the inverse of the function '" 
						+ item.getDescription() + "', the value returned is '" 
						+ convertedValue +"' in '" + item.getType() + "'");
				break;
			} else{
				System.out.println("You entered an incorrect number, please enter either 1 or 2");
			}
		}
	}
	
	private static QAHelper buildSubjectQA(Scanner input, PrintStream output){
		UnitItem EuroToUSD = new UnitItem("Euro", "USD", "Euro = USD * 1.06",new EuroToUSDUnitStrategy());
		UnitItem CelsiusToFahrenheit = new UnitItem("Celsius", "Fahrenheit", "F = (9/5)*C + 32",new CelsiusToFahrenheitUnitStrategy());
		UnitItem PintToGallon = new UnitItem("Pint", "Gallon", "Pint = 0.145 Gallons",new PintToGallonUnitStrategy());
		List<UnitItem> items = new ArrayList<UnitItem>();
		items.add(EuroToUSD);
		items.add(CelsiusToFahrenheit);
		items.add(PintToGallon);
		String question = "Choose which conversion subject you would like to use:";
		QAHelper subjectQA = new QAHelper(input, output, question, items);
		return subjectQA;
	}
}
