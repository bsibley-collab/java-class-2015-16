package com.collaborative.converter.nonfunctional.strategy;

public class EuroToUSDUnitStrategy implements AbstractUnitStrategy{

	@Override
	public double convertTo(double value) {
		return value * 1.06;
	}

	@Override
	public double convertFrom(double value) {
		// TODO Auto-generated method stub
		return value * 0.94;
	}

}
