package com.collaborative.converter.nonfunctional.strategy;

public class PintToGallonUnitStrategy implements AbstractUnitStrategy {

	@Override
	public double convertTo(double value) {
		return value * 0.1454558982684;
	}

	@Override
	public double convertFrom(double value) {
		return value * 6.874936059002;
	}

}
