package com.collaborative.converter.nonfunctional.strategy;

public class CelsiusToFahrenheitUnitStrategy implements AbstractUnitStrategy {
	
	@Override
	public double convertTo(double value){
		return ((9.0/5.0)*value + 32.0);
	}

	@Override
	public double convertFrom(double value) {
		return ((5.0/9.0)*(value-32.0));
	}
}
