package com.collaborative.converter.nonfunctional.strategy;

public interface AbstractUnitStrategy {
	
	public double convertTo(double value);
	
	public double convertFrom(double value);
}
