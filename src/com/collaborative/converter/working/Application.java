package com.collaborative.converter.working;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] temps = {"F-C","C-F","C-K","K-C"};
		//display Conversion Types
		System.out.println("Please select a Temperature conversion type:");
		for ( String temp: temps ){
			System.out.println("->"+temp);
		}
		//get user input
		Scanner sc = new Scanner(System.in);
		String errorMessage = "Please input a whole number (1,2,...)";
		int choice = getChoice(sc,errorMessage);
		
		//display user input
		System.out.println("User chose '" + choice + "'");
		//get user input for temp value
		double temperature = getChoice(sc,"Did not enter in a valid temperature!??");
		System.out.println("You entered '" + temperature + "'");
		//convert that
		if( choice == 1){
			//convert F-C
			
		} else if ( choice == 2){
			//convert C-F
			temperature = (9/5)*temperature+32;
			System.out.println("The converted value is '" + temperature + "'");
		} else if ( choice == 3){
			
		} else if ( choice == 4){
			
		}
		//display converted
	}
	
	private static int getChoice(Scanner sc, String errorMessage) {
		for(;;){
			try{
				int choice = sc.nextInt();
				return choice;
			} catch( InputMismatchException e ){
				System.err.println(errorMessage);
				sc.next();
			}
		}
	}
}
