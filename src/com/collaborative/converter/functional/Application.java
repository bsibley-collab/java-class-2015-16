package com.collaborative.converter.functional;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Covers: Objects, lambda expression (brief intro), Collections (intro - List, ArrayList) 
public class Application {

	public static void main(String[] args){
		//Instantiate our local class variables
		Scanner input = new Scanner(System.in);
		PrintStream output = System.out;
		QAHelper moneyQA = buildMoneyQA(input, output);
		QAHelper temperatureQA = buildTemperatureQA(input, output);
		QAHelper volumeQA = buildVolumeQA(input, output);
		String[] subjects = { "Temperature", "Money", "Volume" };

		//Ask the user to choose a subject
		QAHelper.printAnyList(output, subjects);
		QAHelper choiceQA = new QAHelper(input, output, "Please choose a conversion subject:");
		
		choiceQA.ask();
		int subjectChoice = choiceQA.answerAsInt();
		//Based on the answer, go into the subject and ask them to convert from one unit to another
		switch (subjectChoice){
		case 1:
			convertMenu(input,output,temperatureQA);
			break;
		case 2:
			convertMenu(input,output,moneyQA);
			break;
		case 3:
			convertMenu(input,output,volumeQA);
			break;
		}
	}

	/**
	 * Method that handles any conversion type based on the QAHelper object
	 * @param input
	 * @param applicationQA
	 */
	private static void convertMenu(Scanner input, PrintStream output, QAHelper applicationQA){
		
		//Print list
		applicationQA.printUnitList();
		//Ask user to select a function
		applicationQA.ask();
		//Get user selection and show that we got it
		int selection = applicationQA.answerAsInt();
		int adjustedSelection = selection-1;
		output.println("You entered '" + selection + "'");

		//Ask user for value to convert
		String selectedType = applicationQA.get(adjustedSelection).getType();
		QAHelper converterQA = new QAHelper(input, output, "Please enter a '"+selectedType+"' value to convert:");
		converterQA.ask();
		//Get user value as a double
		double originalValue = converterQA.answerAsDouble();
		//Convert user value
		double convertedValue = applicationQA.convert(adjustedSelection,originalValue);
		//Print out converted value
		output.println("You entered '" + originalValue +"', and after applying the function '" 
				+ applicationQA.get(adjustedSelection).getDescription() + "', the value returned is '" 
				+ convertedValue +"' in '" + applicationQA.get(adjustedSelection).getConvertedToType() + "'");
	}
	
	/**
	 * Our own method to help us create a custom QAHelper object for temperature conversions
	 * @param input - our scanner (console input)
	 * @param output - our console output
	 * @return a new QAHelper object
	 */
	private static QAHelper buildTemperatureQA(Scanner input, PrintStream output){
		UnitItem cToF = new UnitItem("Celsius", //The Unit Type
				"Fahrenheit", //The Unit Type we are converting to
				"F = (9/5)*C + 32",  //The description conversion formula
				c -> ((9.0/5.0)*c + 32.0), //The actual formula using a lambda expression
				f -> ((5.0/9.0)*(f-32.0)) //The formula to convert the other way
				);
		UnitItem fToC = new UnitItem("Fahrenheit", "Celsius", "C = (5/9)*(F-32)", f -> ((5.0/9.0)*(f-32.0)), c -> ((9.0/5.0)*c + 32.0) );
		List<UnitItem> units = new ArrayList<UnitItem>();
		units.add(cToF);
		units.add(fToC); 

		return new QAHelper(input,output,"Please select a conversion type (#):",units);
	}

	private static QAHelper buildMoneyQA(Scanner input, PrintStream output){
		UnitItem euroToUSD = new UnitItem("Euro", "USD", "Euro = 1.06 USD", euro -> euro * 1.06, USD -> USD * 0.94);
		UnitItem USDtoEuro = new UnitItem("USD", "Euro", "USD = 0.94  Euro", USD -> USD * 0.94, euro -> euro * 1.06);
		List<UnitItem> units = new ArrayList<UnitItem>();
		units.add(euroToUSD);
		units.add(USDtoEuro);

		return new QAHelper(input, output,"Please select a conversion type (#):", units);
	}
	
	private static QAHelper buildVolumeQA(Scanner input, PrintStream output){
		UnitItem pintToGallon = new UnitItem("Pint", "Gallon","Pint = 0.145 Gallons", pint -> pint * 0.1454558982684, gallon -> gallon * 6.874936059002 );
		UnitItem gallonToPint = new UnitItem("Gallon", "Pint","Gallon = 6.875 Pints", gallon -> gallon * 6.874936059002, pint -> pint * 0.1454558982684);
		List<UnitItem> units = new ArrayList<UnitItem>();
		units.add(pintToGallon);
		units.add(gallonToPint);

		return new QAHelper(input, output,"Please select a conversion type (#):", units);
	}
}
