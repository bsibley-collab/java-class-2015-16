package com.collaborative.converter.functional;

import java.util.function.Function;

public class UnitItem {

	private String currentType;
	private String convertedToType;
	private String description;
	private Function<Double,Double> functionTo;
	private Function<Double,Double> functionFrom;
	
	public UnitItem(String type, String convertedToType, String description, Function<Double,Double> functionTo, Function<Double,Double> functionFrom){
		this.currentType = type;
		this.convertedToType = convertedToType;
		this.description = description;
		this.functionTo = functionTo;
	}
	
	public double convertTo(double value){
		return functionTo.apply(value);
	}
	
	public double convertFrom(double value){
		return functionFrom.apply(value);
	}
	
	public String getType(){
		return currentType;
	}
	
	public String getConvertedToType(){
		return convertedToType;
	}
	
	public String getDescription(){
		return description;
	}
	
	@Override
	public String toString(){
		return currentType + " : '" + description +"'";
	}
}
