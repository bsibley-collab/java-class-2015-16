package com.collaborative.converter.week3;

import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Please select your conversion Type");
		System.out.println("1: Temperature");
		System.out.println("2: Currency");
		System.out.println("3: Volume");
		Scanner sc = new Scanner(System.in);
		int selection = sc.nextInt();
		Measurement typeO;

		switch (selection) {
		case 1:
			typeO = new Temp();
			break;
		case 2:
			typeO = new Currency();
			break;
		case 3:
			typeO = new Volume();
			break;
		default:
			typeO = null;
			break;
		}
		
		typeO.displayUnits();
		
		System.out.println("Please input a value to convert:");
		
		double value = sc.nextDouble();
		
		typeO.calcResults(value);
	}

}
