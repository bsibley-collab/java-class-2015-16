package com.collaborative.converter.week3.inheritance;

public interface Driveable {
	public void vroom();
}
