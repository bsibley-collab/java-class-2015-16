package com.collaborative.converter.week3.inheritance;

public class Vehicle {
	private String type;
	
	public Vehicle(String type){
		this.type = type;
	}
	
	public String getType(){
		return type;
	}
}
