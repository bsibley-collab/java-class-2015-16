package com.collaborative.converter.week3;

public class Volume implements Measurement {

	@Override
	public void displayUnits() {
		System.out.println("Pints to Gallons: Gallon = 6.875 Pints");
	}

	@Override
	public void calcResults(double from) {
		double result = ((5.0/9.0)*(from-32.0));
		System.out.println("You entered '" + from + "'*F, which is '" + result + "' in Celsius");
	}

}
