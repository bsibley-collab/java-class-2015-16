package com.collaborative.converter.week3;

public interface Measurement {
	public void displayUnits();
	public void calcResults(double from);
}
