package com.collaborative.converter.week3;

public class Currency implements Measurement {

	@Override
	public void displayUnits() {
		System.out.println("USD to Euro: Euro = 1.06 * USD");
	}

	@Override
	public void calcResults(double from) {
		double result = from * 1.06;
		System.out.println("You entered '" + from + "' USD, which is '" + result + "' in Euro");
	}

}
