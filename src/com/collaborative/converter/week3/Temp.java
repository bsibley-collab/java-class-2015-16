package com.collaborative.converter.week3;

public class Temp implements Measurement {

	@Override
	public void displayUnits() {
		System.out.println("Fahrenheit to Celsius: C = (5/9)*(F-32)");
	}

	@Override
	public void calcResults(double from) {
		double result = ((5.0/9.0)*(from-32.0));
		System.out.println("You entered '" + from + "'*F, which is '" + result + "' in Celsius");
	}

}
