package studygroup.jzimmerman;

import studygroup.jzimmerman.units.Unit;
import studygroup.jzimmerman.units.UnitType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class AppData
{
   Map<String, UnitType> types = bootstrap();

   public Collection<UnitType> getUnitTypes() {
      return types.values();
   }

   public Collection<Unit> getUnitsByType(UnitType unitType) {
      return unitType.getUnits();
   }

   public Collection<Unit> getUnitsByTypeExcept(UnitType unitType, Unit unitFrom) {
      return unitType.getUnits().stream()
               .filter(unit -> !unit.equals(unitFrom))
               .collect(Collectors.toSet());
   }

   public static Map<String, UnitType> bootstrap() {
      Map<String, UnitType> types = new HashMap<>();
      types.put("length", UnitType.length());
      types.put("temperature", UnitType.temperature());
      return types;
   }

}
