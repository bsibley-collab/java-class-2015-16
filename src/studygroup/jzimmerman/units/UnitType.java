package studygroup.jzimmerman.units;

import studygroup.jzimmerman.units.length.Centimeter;
import studygroup.jzimmerman.units.length.Inch;
import studygroup.jzimmerman.units.length.Meter;
import studygroup.jzimmerman.units.temperature.Celcius;
import studygroup.jzimmerman.units.temperature.Fahrenheit;
import studygroup.jzimmerman.units.temperature.Kelvin;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class UnitType
{
   public static UnitType length() {
      Set<Unit> units = new HashSet<>();
      units.add(new Inch());
      units.add(new Meter());
      units.add(new Centimeter());

      return new UnitType("length", units);
   }

   public static UnitType temperature() {
      Set<Unit> units = new HashSet<>();
      units.add(new Fahrenheit());
      units.add(new Celcius());
      units.add(new Kelvin());

      return new UnitType("temperature", units);
   }

   public UnitType(String typeName, Set<Unit> units) {
      this.name = typeName;
      this.units = units.stream()
                     .collect(Collectors.toMap(Unit::getUnitName, unit -> unit));
   }

   public Collection<Unit> getUnits() {
      return units.values();
   }

   public Unit getUnit(String name) {
      return units.get(name);
   }

   public String getName() {
      return name;
   }

   @Override
   public String toString() {
      return name;
   }

   private final String name;
   private final Map<String, Unit> units;
}
