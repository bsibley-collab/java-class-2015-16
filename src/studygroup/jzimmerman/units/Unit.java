package studygroup.jzimmerman.units;

public abstract class Unit
{
   public Unit(String unitName) {
      this.unitName = unitName;
   }

   public String getUnitName() { return unitName; }
   public final <T extends Unit> double convertTo(double value, T unitToConvertTo) {
      return unitToConvertTo.convertFromBaseUnit(this.convertToBaseUnit(value));
   }

   public Measurement withValue(double value) {
      return new Measurement(value, this);
   }

   public abstract double convertFromBaseUnit(double value);
   public abstract double convertToBaseUnit(double value);

   @Override
   public String toString() {
      return unitName;
   }

   @Override
   public boolean equals(Object other) {
      return this.getClass().equals(other.getClass());
   }

   @Override
   public int hashCode() {
      return this.getClass().hashCode();
   }

   private final String unitName;
}
