package studygroup.jzimmerman.units;

public final class Measurement
{
   public Measurement(double value, Unit unit) {
      this.value = value;
      this.unit = unit;
   }

   public Measurement convertTo(Unit newUnit) {
      double endValue = unit.convertTo(value, newUnit);
      return new Measurement(endValue, newUnit);
   }

   @Override
   public String toString() {
      return value + " " + unit;
   }

   public final double value;
   public final Unit unit;
}
