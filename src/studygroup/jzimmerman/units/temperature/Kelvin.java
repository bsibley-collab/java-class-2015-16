package studygroup.jzimmerman.units.temperature;

import studygroup.jzimmerman.units.Unit;

public class Kelvin extends Unit
{
   public Kelvin() {
      super("kelvin");
   }

   @Override public double convertFromBaseUnit(double value)
   {
      return value + 273.15;
   }

   @Override public double convertToBaseUnit(double value)
   {
      return value - 273.15;
   }
}
