package studygroup.jzimmerman.units.temperature;

import studygroup.jzimmerman.units.Unit;

public class Fahrenheit extends Unit
{
   public Fahrenheit()
   {
      super("fahrenheit");
   }

   @Override public double convertFromBaseUnit(double value)
   {
      return (value * 9/5) + 32;
   }

   @Override public double convertToBaseUnit(double value)
   {
      return (value - 32) * 5/9;
   }
}
