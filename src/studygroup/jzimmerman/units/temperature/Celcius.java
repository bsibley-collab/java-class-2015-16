package studygroup.jzimmerman.units.temperature;

import studygroup.jzimmerman.units.Unit;

public class Celcius extends Unit
{
   public Celcius() {
      super("celcius");
   }

   @Override public double convertFromBaseUnit(double value)
   {
      return value;
   }

   @Override public double convertToBaseUnit(double value)
   {
      return value;
   }
}
