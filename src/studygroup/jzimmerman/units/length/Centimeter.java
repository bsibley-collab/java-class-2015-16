package studygroup.jzimmerman.units.length;

import studygroup.jzimmerman.units.Unit;

public class Centimeter extends Unit
{
   public Centimeter() {
      super("centimeters");
   }

   @Override public double convertFromBaseUnit(double value)
   {
      return value * 100;
   }

   @Override public double convertToBaseUnit(double value)
   {
      return value / 100;
   }
}
