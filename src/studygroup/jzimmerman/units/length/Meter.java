package studygroup.jzimmerman.units.length;

import studygroup.jzimmerman.units.Unit;

public class Meter extends Unit
{
   public Meter() {
      super("meters");
   }

   @Override public double convertFromBaseUnit(double value)
   {
      return value;
   }

   @Override public double convertToBaseUnit(double value)
   {
      return value;
   }
}
