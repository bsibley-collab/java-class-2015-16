package studygroup.jzimmerman.units.length;

import studygroup.jzimmerman.units.Unit;

public class Inch extends Unit
{
   public Inch()
   {
      super("inches");
   }

   @Override public double convertFromBaseUnit(double value)
   {
      return value / .0254;
   }

   @Override public double convertToBaseUnit(double value)
   {
      return value * .0254;
   }
}
