package studygroup.jzimmerman.vending;

public class ExitItem extends VendingItem
{
   public ExitItem() {
      super("Exit", 0.0f, 1);
   }

   public static boolean isExitItem(Item item) {
      return item instanceof ExitItem;
   }

   @Override public void take() { }
}
