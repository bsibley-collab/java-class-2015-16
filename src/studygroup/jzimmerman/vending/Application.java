package studygroup.jzimmerman.vending;

import studygroup.jzimmerman.vending.ExitItem;
import studygroup.jzimmerman.vending.Purchase;
import studygroup.jzimmerman.vending.VendingMachine;

import java.io.*;

public class Application
{
   public static void main(String[] args) throws IOException
   {
      Application app = new Application(new FileVendingMachineBuilder().get());
      int exit = app.run();
      System.exit(exit);
   }

   final VendingMachine machine;
   final Reader input;
   final Writer output;
   final VendingMachineOutputFormatter formatter;

   public Application(VendingMachine machine) {
      this.machine = machine;
      this.input = new InputStreamReader(System.in);
      this. output = new PrintWriter(System.out);
      this.formatter = VendingMachine::toString;
   }

   public int run()
   {
      try(Reader input = this.input) {
         try(Writer output = this.output) {
            welcomeCustomer(output);
            while(true) {
               Inputs inputs = getChoices();
               if(ExitItem.isExitItem(machine.get(inputs.selection)))
               {
                  returnMoney(inputs.amntPaid);
                  return 0;
               }
               if(machine.isSoldOut(inputs.selection)) {
                  returnMoney(inputs.amntPaid);
                  soldOutMessage(inputs.selection);
               }
               purchaseItem(inputs);
            }
         }
      }
      catch(IOException e)
      {
         e.printStackTrace();
         return 1;
      }
   }

   private void soldOutMessage(int selection)
   {
      //TODO
   }

   private void purchaseItem(Inputs inputs)
   {
      Purchase purchase = machine.purchase(inputs.selection, inputs.amntPaid);
      outputPurchase(purchase);
   }

   private void outputPurchase(Purchase purchase)
   {
      //TODO
   }

   private void returnMoney(float amntPaid)
   {
      //TODO
   }

   private void welcomeCustomer(Writer output) throws IOException
   {
      printWelcomeMessage();
      printChoices();
      output.flush();
   }

   private Inputs getChoices() throws IOException
   {
      while(true) {
         Inputs choices = new Inputs(getPayment(), getSelection());
         if (!machine.has(choices.selection)) {
            output.write("ERR: Bad Selection. Item is either sold out or is invalid");
            output.flush();
         }
         else if(machine.priceOf(choices.selection) > choices.amntPaid) {
            output.write("ERR: Not enough money. Paid: " +choices.amntPaid+ ", Price: " +machine.priceOf(choices.selection));
         }
         else {
            return choices;
         }
      }
   }

   private int getSelection()
   {
      //TODO
      return 0;
   }

   private float getPayment()
   {
      //TODO
      return 0.0f;
   }

   private void printChoices() throws IOException
   {
      output.write(formatter.apply(machine));
   }

   private void printWelcomeMessage() throws IOException
   {
      output.write("Welcome to the vending machine! Please make a selection from the following items.");
   }
}
