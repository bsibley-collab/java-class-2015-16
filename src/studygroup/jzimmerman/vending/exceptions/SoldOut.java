package studygroup.jzimmerman.vending.exceptions;

import studygroup.jzimmerman.vending.Item;

public class SoldOut extends RuntimeException
{
   public SoldOut(Item item){
      super(item.name() + " is sold out");
   }
}
