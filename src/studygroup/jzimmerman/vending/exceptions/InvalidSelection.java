package studygroup.jzimmerman.vending.exceptions;

public class InvalidSelection extends RuntimeException
{
   public InvalidSelection(int selection) {
      super("There is no option for " + selection);
   }
}
