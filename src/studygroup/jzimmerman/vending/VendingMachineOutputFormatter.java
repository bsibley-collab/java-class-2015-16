package studygroup.jzimmerman.vending;

import studygroup.jzimmerman.vending.VendingMachine;
import java.util.function.Function;

@FunctionalInterface
public interface VendingMachineOutputFormatter
         extends Function<VendingMachine, String> { }
