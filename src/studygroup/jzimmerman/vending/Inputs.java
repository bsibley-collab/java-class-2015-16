package studygroup.jzimmerman.vending;

public class Inputs
{
   public final float amntPaid;
   public final int selection;

   public Inputs(float amntPaid, int selection) {
      this.amntPaid = amntPaid;
      this.selection = selection;
   }
}
