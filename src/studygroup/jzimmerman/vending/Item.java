package studygroup.jzimmerman.vending;

public interface Item
{
   public String name();
   public float cost();
   public int quantity();
}
