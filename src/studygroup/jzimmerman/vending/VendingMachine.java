package studygroup.jzimmerman.vending;

import studygroup.jzimmerman.vending.exceptions.InvalidSelection;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.TreeSet;

public class VendingMachine
{
   private final Map<Integer, VendingItem> options;

   VendingMachine(Map<Integer, VendingItem> options) {
      this.options = options;
   }

   public float priceOf(int selection)
   {
      return get(selection).cost();
   }

   public boolean has(int selection)
   {
      return options.containsKey(selection);
   }

   public Item get(int selection)
   {
      if(has(selection))
         return options.get(selection);
      else
         throw new InvalidSelection(selection);
   }

   public Purchase purchase(int selection, float amntPaid)
   {
      VendingItem item = options.get(selection);
      if(item.cost() > amntPaid)
         return new Purchase("Nothing - not enough money paid", amntPaid);
      else {
         item.take();
         return new Purchase(item.name(), amntPaid - item.cost());
      }
   }

   public boolean isSoldOut(int selection) {
      return get(selection).quantity() == 0;
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      GenericFormat formatter = new GenericFormat();

      sb.append("Item#   Name       Price     Qty\n");
      sb.append("--------------------------------\n");

      for( Map.Entry<Integer, VendingItem> entry : new TreeSet<>(options.entrySet())) {
         sb.append(formatter.formatObj(entry.getKey(), 8));
         VendingItem item = entry.getValue();
         sb.append(formatter.formatObj(item.name(), 12));
         sb.append("$");
         sb.append(formatter.formatObj(item.cost(), 9));
         sb.append(formatter.formatObj(item.quantity(), 5));
         sb.append("\n");
      }

      return sb.toString();
   }
}

class GenericFormat {

   public String formatObj(Object obj, int totalWidth) {
      String strObj = getText(obj);
      int diff = totalWidth - strObj.length();

      StringBuilder sb = new StringBuilder(strObj);
      addSpaces(diff, sb);

      return sb.toString();
   }

   private String getText(Object obj) {
      if (obj instanceof Float) {
         DecimalFormat df = new DecimalFormat("###0.00");
         return df.format(((Float) obj).floatValue());
      }
      else
         return obj.toString();
   }

   private void addSpaces(int n, Appendable appender) {
      for (int i = 1; i <= n; i++)
         try {
            appender.append(" ");
         }
         catch(IOException e)  {
            e.printStackTrace();
         }
   }

}