package studygroup.jzimmerman.vending;

public class Purchase
{
   public final String item;
   public final float change;

   Purchase(String item, float change)
   {
      this.item = item;
      this.change = change;
   }
}
