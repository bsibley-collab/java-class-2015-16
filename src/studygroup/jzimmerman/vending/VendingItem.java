package studygroup.jzimmerman.vending;

import studygroup.jzimmerman.vending.exceptions.SoldOut;

class VendingItem implements Item
{
   public VendingItem(String name, float cost, int quantity) {
      this.name = name;
      this.cost = cost;
      this.quantity = quantity;
   }

   public String name() { return name; }

   public float cost() { return cost; }

   public int quantity() { return quantity; }

   void take() throws SoldOut {
      if(quantity <= 0)
         throw new SoldOut(this);
      else
         quantity--;
   }

   private int quantity;
   private final float cost;
   private final String name;
}
